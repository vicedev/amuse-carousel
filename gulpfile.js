 /*!
  * gulpfile.js
  * v0.0.2
  *
  * Copyright VICE Media inc UK
  * Released under the MIT license
  */

var gulp = require('gulp');
var stylus = require('gulp-stylus');
var nib = require('nib');
var concat = require('gulp-concat');
var notify = require('gulp-notify');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var babelify= require('babelify');
var util = require('gulp-util');

// Stylus
gulp.task('stylus', function() {
  return gulp.src('./src/css/index.styl')
    .pipe(stylus({
      use: nib(),
      // compress: true,
      errors: true
    }))
    .pipe(concat('build.css'))
    .pipe(gulp.dest('./public/css'));

});

// Browser sync
gulp.task('browser-sync', function() {

  var watchFiles = [
    'public/css/*',
    'public/js/*',
    'src/templates/**/*'
  ];

  browserSync.init(watchFiles, {
      proxy: 'localhost:8080',
      notify: false,
      scrollProportionally: false
  });
  
});

// JS
gulp.task('js', function () {

  browserify('./src/js/app.js', { debug: true })
    .add(require.resolve('babel/polyfill'))
    .transform(babelify.configure({
      // Ignore large packages
      ignore: 'jquery'
    }))
    .transform(babelify)
    .bundle()
    .on('error', util.log.bind(util, 'Browserify Error'))
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(uglify({ mangle: false }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./public/js'));

});

// Watch task
gulp.task('watch', ['stylus', 'js', 'browser-sync'], function() {

  gulp.watch([
    './src/css/*.styl',
    './src/css/**/*.styl'
  ], ['stylus']);

  gulp.watch([
    './src/js/*.js',
    './src/js/**/*.js'
  ], ['js']);

});

gulp.task('default', ['stylus', 'js', 'browser-sync']);

// server.js

// SETUP
var express = require('express');
var app = express();
var port = process.env.PORT || 8080;
var exphbs = require('express-handlebars');

// Set views
app.set('views', __dirname + '/views');

// Set up templating engine
app.engine('handlebars', exphbs({ 
	defaultLayout: __dirname + '/views/layouts/main', 
	partialsDir: __dirname + '/views/partials'
}));

app.set('view engine', 'handlebars');

app.use('/public', express.static(__dirname + '/../public'));

app.get('/', function(req, res) {

	res.render('pages/index');

});

app.listen(port);
console.log('Magic happens on port ' + port);
(function ($) {
	"use strict";

	$(function () {

		var $articleCarousels = $('[data-carousel]');

		$articleCarousels.each(function(i, articleCarousel) {

			var $articleCarousel = $(articleCarousel),
					$carousel = $articleCarousel.find('[data-carousel-items]'),
					$carouselItems = $articleCarousel.find('.article-carousel__item'),
					$status = $articleCarousel.find('[data-carousel-counter]'),
					$arrows = $articleCarousel.find('[data-carousel-arrows]'),
					$coverImage = $articleCarousel.find('[data-carousel-cover]'),
					$images = $articleCarousel.find('[data-carousel-image]'),
					section = $articleCarousel.data('carousel'),
					$top10;

			switch(section) {
				case 'homepage':
				case 'section-homepage':
					$images.each(function (i, image) {
						var $image = $(image),
								$img = $image.find('img'),
								src = $img.attr('src');
						$image.css({ 
							'background' : 'url('+src+')',
							'background-size' : 'cover',
							'background-postition ': 'center center'
						});

						$img.remove();
					})
					break;
				case 'top-10':
					$top10 = $('[data-top-10]');
					var $top10Item = $top10.find('li'),
							tabIndex = 0;
					$top10Item.each(function (i, item) {
						$(item).attr('data-tab', tabIndex);
						tabIndex++;
					})
					break;
			}

			// Switch top 10 content
			var switchTop10 = function (target) {
				$top10.find('li').removeClass('selected');
				$top10.find('[data-tab="'+target+'"]').addClass('selected');
			}

			$carousel.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){

				$articleCarousel.addClass(section)

				if($carouselItems.length > 1){
					var i = (currentSlide ? currentSlide : 0) + 1;
		    	$status.text(i + ' of ' + slick.slideCount);
		    } else {
		    	$coverImage.addClass('hidden');
		    }

			});

			$carousel.on('beforeChange', function(event, slick, currentSlide, nextSlide){
				if(section == 'top-10') {
					switchTop10(nextSlide);
				}
			});

			var carousel = $carousel.slick({
				appendArrows: $arrows,
				prevArrow: '<div class="bx-prev prev"><</div>',
				nextArrow: '<div class="bx-next next">></div>'
			});

			$arrows.add($coverImage).on('click', function (e) {
				$coverImage.fadeOut('fast');
			})

		});

	});

}(jQuery));
